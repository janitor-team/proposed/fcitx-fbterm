Source: fcitx-fbterm
Section: utils
Priority: optional
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders:
 Aron Xu <aron@debian.org>,
 YunQiang Su <syq@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 fcitx-libs-dev (>= 1:4.2.6),
 gettext,
 libdbus-glib-1-dev,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://github.com/fcitx/fcitx-fbterm
Vcs-Git: https://salsa.debian.org/input-method-team/fcitx-fbterm.git
Vcs-Browser: https://salsa.debian.org/input-method-team/fcitx-fbterm

Package: fcitx-frontend-fbterm
Architecture: linux-any
Depends:
 fbterm,
 fcitx-bin,
 fcitx-data,
 fcitx-modules,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 fcitx-googlepinyin | fcitx-sunpinyin,
 fcitx-pinyin,
 fcitx-table-wubi | fcitx-table-wbpy,
Suggests:
 fcitx-tools,
Description: Flexible Input Method Framework - FbTerm frontend
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides the FbTerm frontend, which is recommended for
 users who does not use X.
